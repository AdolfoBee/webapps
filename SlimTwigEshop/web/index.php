<?php

session_cache_limiter(false);
session_start();

// this is how to retrieve current session ID string
$sessionID = session_id();

require_once '/../vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

DB::$dbName = 'eshopdb';
DB::$user = 'eshopdb';
DB::$password = '9fHpBEkpTPszG3Oe';
DB::$port = 3306;

DB::$error_handler = 'sql_error_handler';
DB::$nonsql_error_handler = 'nonsql_error_handler';

function nonsql_error_handler($params) {
    global $app, $log;
    $log->error("Database error: " . $params['error']);
    http_response_code(500);
    $app->render('error_internal.html.twig');
    die;
}

function sql_error_handler($params) {
    global $app, $log;
    $log->error("SQL error: " . $params['error']);
    $log->error(" in query: " . $params['query']);
    http_response_code(500);
    $app->render('error_internal.html.twig');
    die;
}

$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/../cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/../templates');

\Slim\Route::setDefaultConditions(array(
    'productID' => '\d+',
    'categoryID' => '\d+',
    'cartID'=> '\d+'
));

if(!isset($_SESSION['user'] ))
{
    $_SESSION['user'] = array('name' => '');
}
$quantities = DB::queryOneColumn('quantity', "SELECT quantity FROM cart_item WHERE sessionID=%s", $sessionID);
$quantityTotal=0;
foreach ($quantities as $quan) {
    $quantityTotal=$quantityTotal + $quan;
}
$view->getEnvironment()->addGlobal('quan', $quantityTotal);
//-----------------------MAIN--------------------------------------------------------------------------------------------
$app->get('/(main)', function () use ($app) {
    
    $productList = DB::query("SELECT * FROM product WHERE isFrontPage=1");
    $categoryList = DB::query("SELECT id, name FROM category");
    
    $app->render('main.html.twig', array(
    'categoryList'=>$categoryList,
    'productList'=>$productList,
    'user' => $_SESSION['user']['name']
    ));
});

//-----------------------LOGIN---------------------------------------------------------------------------------------------
$app->get('/login', function () use ($app) {
    $username='admin';
    $password='12345';
    $categoryList = DB::query("SELECT id, name FROM category");
    $app->render('login.html.twig', array(
        'username'=>$username,
        'password'=>$password,
        'user' => $_SESSION['user']['name'],
        'categoryList'=>$categoryList
    ));
});

$app->post('/login', function () use ($app) {
    $username = $app->request()->post('name');
    $password = $app->request()->post('pass');
    $categoryList = DB::query("SELECT id, name FROM category");
    if($username=='admin' && $password=='12345')
    {
        $_SESSION['user'] = array('name' => $username);
        $app->redirect('/');
    }
    else
    {
        $app->render('error_internal.html.twig', array(
        'user' => $_SESSION['user']['name'],
        'categoryList'=>$categoryList
        ));
    }
});
//-----------------------LOGOUT-------------------------------------------------------------------------------------------

$app->get('/logout', function () use ($app) {
    unset($_SESSION['user']);
    $_SESSION['user'] = array('name' => '');
    $categoryList = DB::query("SELECT id, name FROM category");
    $app->render('logout.html.twig', array(
        'user' => $_SESSION['user']['name'],
        'categoryList'=>$categoryList
    ));
});

//-----------------------FORBIDDEN----------------------------------------------------------------------------------------

$app->get('/forbidden', function () use ($app) {
    $categoryList = DB::query("SELECT id, name FROM category");
    $app->render('forbidden.html.twig', array(
        'user' => $_SESSION['user']['name'],
        'categoryList'=>$categoryList
    ));
});

//-----------------------ORDER--------------------------------------------------------------------------------------------

$app->get('/order', function () use ($app, $sessionID) {
    
    $price=DB::query("SELECT product.price, cart_item.quantity FROM cart_item, product WHERE product.id=cart_item.productID"
            . " AND cart_item.sessionID=%s", $sessionID);
    
    $pretotal=0;
    $total=0;
    foreach ($price as $p) {
        $pretotal=$pretotal+($p['price']*$p['quantity']);
    }
    $total=($pretotal*1.12)+15;
    
    $categoryList = DB::query("SELECT id, name FROM category");
    $app->render('order.html.twig', array(
    'categoryList'=>$categoryList,
    'user' => $_SESSION['user']['name'],
    'pretotal' => $pretotal,
    'total' => $total
    ));
});

$app->post('/order', function ($productID=0) use ($app, $sessionID) {
    // extract variables
    $firstName = $app->request()->post('firstName');
    $lastName = $app->request()->post('lastName');
    $address = $app->request()->post('address');
    $postCode = $app->request()->post('postCode');
    $country = $app->request()->post('country');
    $province = $app->request()->post('province');
    $email = $app->request()->post('email');
    $cemail = $app->request()->post('cemail');
    $phone = $app->request()->post('phone');
    $cnum = $app->request()->post('cnum');
    $cex = $app->request()->post('cex');
    $ccvv = $app->request()->post('ccvv');
    
    $price=DB::query("SELECT product.price, cart_item.quantity FROM cart_item, product WHERE product.id=cart_item.productID"
            . " AND cart_item.sessionID=%s", $sessionID);
    
    $pretotal=0;
    $total=0;
    $taxes=0;
    foreach ($price as $p) {
        $pretotal=$pretotal+($p['price']*$p['quantity']);
    }
    $taxes=$pretotal*0.12;
    $total=($pretotal*1.12)+15;
    
    $errorList = array();
    $valueList = array(
        'firstName' => $firstName,
        'lastName' => $lastName,
        'address' => $address,
        'postCode'=>$postCode,
        'country'=>$country,
        'province'=>$province,
        'email'=>$email,
        'cemail'=>$cemail,
        'phone'=>$phone,
        'cnum'=>$cnum,
        'cex'=>$cex,
        'ccvv'=>$ccvv,
        'pretotal'=>$pretotal,
        'total'=>$total,
        );
    
    $categoryList = DB::query("SELECT id, name FROM category");
    
    if (strlen($firstName) > 50 || strlen($firstName) < 3) {
        array_push($errorList, "First Name length must between 3 and 50 characters.");
        $valueList['firstName'] = '';
    }
    if (strlen($lastName) > 50 || strlen($lastName) < 3) {
        array_push($errorList, "Last name length must between 3 and 50 characters.");
        $valueList['lastName'] = '';
    }
    if (strlen($address) > 100 || strlen($address) < 5) {
        array_push($errorList, "Address length must be between 5 and 100 characters.");
        $valueList['address'] = '';
    }
    if (strlen($postCode) > 10 || strlen($postCode) < 2) {
    array_push($errorList, "Postal Code length must be between 2 and 10 characters.");
    $valueList['postalCode'] = '';
    }
    if (strlen($country) > 50 || strlen($country) < 2) {
    array_push($errorList, "Country length must be between 2 and 50 characters.");
    $valueList['country'] = '';
    }
    if (strlen($province) > 20 || strlen($province) < 2) {
    array_push($errorList, "Province length must be between 2 and 20 characters.");
    $valueList['province'] = '';
    }
    if(filter_var($email, FILTER_VALIDATE_EMAIL)===FALSE) {
        array_push($errorList, "Email looks invalid.");
        $valueList['email']= '';
        $valueList['cemail']= '';
    }
    if($cemail!=$email) {
        array_push($errorList, "The email and email confirmation does not match");
        $valueList['email']= '';
        $valueList['cemail']= '';
    }
    if (strlen($phone) > 20 || strlen($phone) < 2) {
    array_push($errorList, "Phone length must be between 2 and 20 characters.");
    $valueList['phone'] = '';
    }
    if (strlen($cnum) > 20 || strlen($cnum) < 10) {
    array_push($errorList, "Credit Card Number length must be between 10 and 20 characters.");
    $valueList['cnum'] = '';
    }
    if(empty($cex)){
        array_push($errorList, "Please provide an experity date. Please use MM-YYYY");
    }else{
        $test = DateTime::createFromFormat('m-Y', $cex);
        if ($test && $test->format('m-Y') == $cex) {}else{
        array_push($errorList, "Date looks invalid. Please use MM-YYYY");
        $valueList['cex'] = '';
        }
    }
    if (strlen($ccvv) > 6 || strlen($ccvv) < 2) {
    array_push($errorList, "CVV length must be between 2 and 6 characters.");
    $valueList['ccvv'] = '';
    }
    
    if ($errorList) {
        // STATE 2: failed submission
        $app->render('order.html.twig', array(
            'errorList' => $errorList,
            'v' => $valueList,
            'user' => $_SESSION['user']['name'],
            'categoryList'=>$categoryList,
            'pretotal' => $pretotal,
            'total' => $total
        ));
    } else {
        // STATE 3: successful submission

            DB::$error_handler = FALSE;
            DB::$throw_exception_on_error = TRUE;
            // PLACE THE ORDER
            try {
                DB::startTransaction();
                // 1. create summary record in 'orders' table (insert)
                DB::insert('order_header', array(
                'first_name'=>$firstName,
                'last_name'=>$lastName,
                'address'=>$address,
                'postcode'=>$postCode, 
                'country'=>$country,
                'provinceorstate'=>$province,
                'email'=>$email,
                'phone'=>$phone,
                'credit_card_no'=>$cnum,
                'credit_card_expirity'=>$cex,
                'credit_card_cvv'=>$ccvv,
                'total_before_tax_and_delivery'=>$pretotal,
                'delivery'=>15,
                'taxes'=>$taxes,
                'total_final'=>$total
                ));

               $orderID = DB::insertId();

               // 2. copy all records from cartitems to 'orderitems' (select & insert)
               $cartitemList = DB::query("SELECT category.name as catName, product.name as prodName, product.description, "
                       . "product.image_path, product.price, cart_item.quantity FROM cart_item, product, category "
                       . "WHERE product.id=cart_item.productID AND cart_item.sessionID=%s "
                       . "AND category.id=product.categoryID", $sessionID);


               // insert all items 
               
               foreach ($cartitemList as $item) {
                    DB::insert('order_item', array(
                        'orderheaderID' => $orderID,
                        'category_name' => $item['catName'],
                        'name' => $item['prodName'],
                        'description' => $item['description'],
                        'image_path' => $item['image_path'],
                        'unit_price' => $item['price'],
                        'quantity' => $item['quantity']
                    ));
               }
                
              // 3. delete cartitems for this user's session (delete)
              DB::delete('cart_item', "sessionID=%s", $sessionID);
              DB::commit();

            $app->render('order_success.html.twig', array(
                'categoryList'=>$categoryList,
                'user' => $_SESSION['user']['name']
            ));
            } catch (MeekroDBException $e) {
              DB::rollback();
              sql_error_handler(array(
                'error' => $e->getMessage(),
                'query' => $e->getQuery()
              ));
            }
    }
});

//-----------------------CATEGORY------------------------------------------------------------------------------------------

$app->get('/category/:categoryID', function ($categoryID=0) use ($app, $sessionID) {
    $productList2 = DB::query("SELECT * FROM cart_item, product WHERE cart_item.productID=product.id AND cart_item.sessionID=%s", $sessionID);
    
    $categoryList = DB::query("SELECT id, name FROM category");
    $productList = DB::query("SELECT * FROM product WHERE categoryID=%i", $categoryID);
    
    if(!empty($productList))
    {
        $app->render('product_list.html.twig', array(
        'productList' => $productList,
        'categoryList' => $categoryList,
        'productList2' => $productList2,
        'user' => $_SESSION['user']['name']
        ));
    }
    else{
        $app->redirect('/');
    }
});

//-----------------------PRODUCT-------------------------------------------------------------------------------------------

$app->get('/product/:productID', function ($productID=0) use ($app) {
    $categoryList = DB::query("SELECT id, name FROM category");
    $productList = DB::query("SELECT * FROM product WHERE id=%i", $productID);
    if(!empty($productList))
    {
        $app->render('product.html.twig', array(
        'productList' => $productList,
        'user' => $_SESSION['user']['name'],
        'categoryList'=>$categoryList
        ));
    }
    else{
        $app->redirect('/');
    }
});

//-----------------------CART-----------------------------------------------------------------------------------------------

$app->get('/cart', function () use ($app, $sessionID) {
    
    $productList = DB::query("SELECT cart_item.id AS cartID, cart_item.productID, cart_item.quantity, cart_item.sessionID, "
            . "cart_item.whenCreated, product.description, product.id, product.image_path, product.isFrontPage, "
            . "product.name, product.price  FROM cart_item, product WHERE cart_item.productID=product.id "
            . "AND cart_item.sessionID=%s", $sessionID);
    $categoryList = DB::query("SELECT id, name FROM category");
    
    $app->render('cart.html.twig', array(
    'productList' => $productList,
    'categoryList' => $categoryList,
    'user' => $_SESSION['user']['name']
    ));
});

//-----------------------CART/ADD------------------------------------------------------------------------------------------

$app->get('/cart/add/:productID', function ($productID=0) use ($app, $sessionID) {
    
    $categoryID = DB::queryFirstField("SELECT categoryID FROM product WHERE id=%i", $productID);
    DB::insert('cart_item', array('sessionID'=> $sessionID, 'productID'=>$productID, 'quantity'=>1));
    
    $app->redirect('/category/'. $categoryID);
});

//-----------------------CART/DELETE---------------------------------------------------------------------------------------

$app->get('/cart/delete/:productID', function ($productID=0) use ($app) {
    
    DB::delete('cart_item', "productID=%i", $productID);
    
    $app->redirect('/cart');
});

//-----------------------AJAX/CART/SET/PROD--------------------------------------------------------------------------------

$app->get('/ajax/cart/set/prod/:cartID/quantity/:quantity', function ($cartID=0, $quantity=0) use ($app) {
    
    DB::update('cart_item', array(
    'quantity' => $quantity
    ), "id=%i", $cartID);
});

//-----------------------ADMIN/PRODUCT/DELETE------------------------------------------------------------------------------

$app->get('/admin/product/delete/:productID', function ($productID=0) use ($app) {
    if($_SESSION['user']['name'] != 'admin')
    {
        $app->redirect('/forbidden');
    }
    else
    {
        DB::delete('product', "id=%i", $productID);
        $app->redirect('/admin/product/list');
    }
});

//-----------------------ADMIN/CATEGORY/LIST-------------------------------------------------------------------------------

$app->get('/admin/category/list', function () use ($app) {
    if($_SESSION['user']['name'] != 'admin')
    {
        $app->redirect('/forbidden');
    }
    else
    {
        $categoryList = DB::query("SELECT id, name FROM category");
        $app->render('category_list.html.twig', array(
        'categoryList'=>$categoryList,
        'user' => $_SESSION['user']['name']
        ));
    }
});

//-----------------------ADMIN/CATEGORY/ADDEDIT----------------------------------------------------------------------------

$app->get('/admin/category/addedit(/)(:categoryID)', function ($categoryID=0) use ($app) {
    if($_SESSION['user']['name'] != 'admin')
    {
        $app->redirect('/forbidden');
    }
    else
    {
        $check=false;
        $catID=DB::queryFirstColumn("SELECT id FROM category");
        foreach ($catID as $id) {
            if($id==$categoryID){
                $check=true;
            }
        }
        $categoryList = DB::query("SELECT id, name FROM category");
        $categoryNames = DB::queryFirstColumn("SELECT name FROM category");
        if ($categoryID!=0 && $check) 
        {
            //print_r($catID);
            //echo $categoryID;
            $category = DB::queryFirstRow("SELECT id, name FROM category WHERE id=%i", $categoryID);
            $app->render('category_add_edit.html.twig', array(
            'categoryName'=>$category['name'],
            'categoryID'=>$category['id'],
            'categoryNames'=>$categoryNames,
            'categoryList'=>$categoryList,
            'user' => $_SESSION['user']['name']
            ));
        }
        else
        {
            if(!$check && $categoryID!=0)
            {
                $app->redirect('/admin/category/addedit'); 
            }
            else
            {
                $app->render('category_add_edit.html.twig', array(
                'categoryNames'=>$categoryNames,
                'categoryList'=>$categoryList,
                'user' => $_SESSION['user']['name']
                ));
            }
        }
    }
});

$app->post('/admin/category/addedit(/)(:categoryID)', function ($categoryID=0) use ($app) {
    // extract variables
    $name = $app->request()->post('name');
    $valueList = array('name' => $name);
    $errorList = array();
    $addedit=0;
    $categoryList = DB::query("SELECT id, name FROM category");
    $checkcategory=DB::queryFirstField("SELECT name FROM category WHERE name=%s", $name);
    
    if ($checkcategory)
    {
        array_push($errorList, "Category already exist.");
        $valueList['name'] = '';
    }
    
    if (strlen($name) > 50) {
        array_push($errorList, "Category name length must be less than 50 characters.");
        $valueList['name'] = '';
    }
    
    if ($errorList) {
        // STATE 2: failed submission
        $app->render('category_add_edit.html.twig', array(
            'errorList' => $errorList,
            'v' => $valueList,
            'user' => $_SESSION['user']['name'],
            'categoryList'=>$categoryList
        ));
    } else {
        // STATE 3: successful submission
        if($categoryID!=0)
        {
            DB::update('category', array('name' => $name), "id=%i", $categoryID);
            $addedit=1;
        }
        else{
            DB::insert('category', array('name' => $name));
            $addedit=0;
        }
        $app->render('category_add_edit_success.html.twig', array(
            'addedit'=>$addedit,
            'user' => $_SESSION['user']['name'],
            'categoryList'=>$categoryList
            ));
    }
    
});

//-----------------------ADMIN/PRODUCT/LIST-------------------------------------------------------------------------------

$app->get('/admin/product/list', function () use ($app) {
    if($_SESSION['user']['name'] != 'admin')
    {
        $app->redirect('/forbidden');
    }
    else
    {
        $categoryList = DB::query("SELECT id, name FROM category");
        $productList = DB::query("SELECT * FROM product");
        $app->render('product_list.html.twig', array(
        'productList'=>$productList,
        'user' => $_SESSION['user']['name'],
        'categoryList'=>$categoryList
        ));
    }
});

//-----------------------ADMIN/PRODUCT/ADDEDIT-----------------------------------------------------------------------------

$app->get('/admin/product/addedit(/)(:productID)', function ($productID=0) use ($app) {
    if($_SESSION['user']['name'] != 'admin')
    {
        $app->redirect('/forbidden');
    }
    else
    {
        $check=false;
        $proID=DB::queryFirstColumn("SELECT id FROM product");
        foreach ($proID as $id) {
            if($id==$productID){
                $check=true;
            }
        }
        $categoryList = DB::query("SELECT id, name FROM category");
        $categoryNames = DB::queryFirstColumn("SELECT name FROM category");
        if ($productID!=0 && $check) 
        {
            $product = DB::queryFirstRow("SELECT category.name AS categoryName, product.name, product.description, product.price, product.image_path "
                                       . "FROM product, category WHERE product.id=%i AND category.id=product.categoryID;", $productID);
            $valueList = array('categoryName'=>$product['categoryName'],
                               'name' => $product['name'],
                               'description' => $product['description'],
                               'price' => $product['price'],
                               'image_path'=>$product['image_path']);
            $app->render('product_add_edit.html.twig', array(
            'v' => $valueList,
            'user' => $_SESSION['user']['name'],
            'categoryNames'=>$categoryNames,
            'categoryList'=>$categoryList
            ));
        }
        else
        {
            if(!$check && $productID!=0)
            {
                $app->redirect('/admin/product/addedit'); 
            }
            else
            {
                $app->render('product_add_edit.html.twig', array(
                'user' => $_SESSION['user']['name'],
                'categoryNames'=>$categoryNames,
                'categoryList'=>$categoryList
                ));
            }
        }
    }
});

$app->post('/admin/product/addedit(/)(:productID)', function ($productID=0) use ($app) {
    // extract variables
    $name = $app->request()->post('name');
    $description = $app->request()->post('description');
    $price = $app->request()->post('price');
    $category = $app->request()->post('category');
    $frontPage = $app->request()->post('fp');
    $imageUpload = $_FILES['imageUpload'];
    $imageName = $imageUpload['name'];
    $addedit=0;
    $valueList = array('name' => $name, 'description' => $description, 'price' => $price, 'categoryName'=>$category);
    $errorList = array();
    
    $image= DB::queryFirstField("SELECT image_path FROM product WHERE id=%i", $productID);
    $categoryList = DB::query("SELECT id, name FROM category");
    $categoryID=DB::queryFirstField("SELECT id FROM category WHERE name=%s", $category);
    
    if (strlen($name) > 100 || strlen($name) < 3) {
        array_push($errorList, "Product name length must between 3 and 100 characters.");
        $valueList['name'] = '';
    }
    if (strlen($description) > 1000 || strlen($description) < 5) {
        array_push($errorList, "Description length must be between 5 and 1000 characters.");
        $valueList['description'] = '';
    }
    if(empty($price))
    {
        array_push($errorList, "Provide a price for the product.");
    }
    if($category=="none")
    {
        array_push($errorList, "Provide a category for the product.");
    }
    if ($imageUpload['error'] != 0 && empty($image)) {
        array_push($errorList, "Error uploading image, code " . $imageUpload['error']);
    }
    else if ($imageUpload['error'] == 0 && empty($image)){
        $info = getimagesize($imageUpload["tmp_name"]);
        if ($info == FALSE) {
        array_push($errorList, "Error uploading image, it doesn't look like a valid image file");
        }
    }
    
    if ($errorList) {
        // STATE 2: failed submission
        $app->render('product_add_edit.html.twig', array(
            'errorList' => $errorList,
            'v' => $valueList,
            'user' => $_SESSION['user']['name'],
            'categoryList'=>$categoryList
        ));
    } else {
        // STATE 3: successful submission
        if($imageUpload['error'] != 0 && !empty($image))
        {
            $imagePath=$image;
        }
        else
        {
            $info = getimagesize($imageUpload["tmp_name"]);
            switch ($info['mime']) 
            {
                case 'image/jpeg': $ext = '.jpg'; break;
                default: $ext = "." . explode("/", $info['mime'])[1];
            }
            $imageName=md5($name.time()) . $ext;
            move_uploaded_file($imageUpload['tmp_name'], 'uploads/' . $imageName);
            $imagePath='/uploads/' . $imageName;
        }
        $f=0;
        if($frontPage=='yes')
        {
            $f=1;
        }
        
        if($productID!=0)
        {
            DB::update('product', array(
                'categoryID'=>$categoryID,
                'name'=>$name,
                'description'=>$description,
                'image_path'=>$imagePath, 
                'price'=>$price,
                'isFrontPage'=>$f
            ), "id=%i", $productID);
            $addedit=1;
        }
        else
        {
            DB::insert('product', array(
                'categoryID'=>$categoryID,
                'name'=>$name,
                'description'=>$description,
                'image_path'=>$imagePath, 
                'price'=>$price,
                'isFrontPage'=>$f
            ));
            $addedit=0;
        }
        $app->render('product_add_edit_success.html.twig', array(
            'addedit'=>$addedit,
            'user' => $_SESSION['user']['name'],
            'categoryList'=> $categoryList
            ));
    }
});

//-----------------------LOGIN-------------------------------------------------------------------------------------------

$app->run();