<?php

/* forbidden.html.twig */
class __TwigTemplate_377a2094102f0aa65992a2ce1bd31e3da9acda6948432c1460109ff9c46f1d3d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "forbidden.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Forbidden";
    }

    // line 5
    public function block_mainContent($context, array $blocks = array())
    {
        // line 6
        echo "    <span> Page accessible to authorized users only. You must <a href=\"/login\"> log in</a> to access it.</span>
";
    }

    public function getTemplateName()
    {
        return "forbidden.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Forbidden{% endblock %}

{% block mainContent %}
    <span> Page accessible to authorized users only. You must <a href=\"/login\"> log in</a> to access it.</span>
{% endblock %}", "forbidden.html.twig", "C:\\xampp\\htdocs\\webapps\\hw2eshop\\templates\\forbidden.html.twig");
    }
}
