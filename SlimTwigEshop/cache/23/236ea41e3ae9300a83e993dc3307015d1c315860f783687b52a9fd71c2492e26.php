<?php

/* master.html.twig */
class __TwigTemplate_750e2f26d6879e31627c6bdaf22653043623d9ddcc2c3ebf4109bc5a9f0753d0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'headExtra' => array($this, 'block_headExtra'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        echo "<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta charset=\"utf-8\">
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/style.css\">
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js\"></script>
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\">
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css\">
        <link rel=\"stylesheet\" href=\"/plugin/menu/dist/sidebar-menu.css\">
        <script src=\"/plugin/menu/dist/sidebar-menu.js\"></script>
        <script>
            \$(document).ready(function () {
                \$.sidebarMenu(\$('.sidebar-menu'));
            });
        </script>
    ";
        // line 19
        $this->displayBlock('headExtra', $context, $blocks);
        // line 20
        echo "</head>
<body style=\"background: url('http://localhost:8003/bg4.jpg') #E0CCD2 repeat center center fixed;\">

    <div class=\"container black\" style=\"min-height: 1080px\">
        <div class=\"quarter\" style=\"min-height: 1080px\">
            <div class=\"padded\">
                <section class=\"section\">
                    <ul class=\"sidebar-menu\">
                        <li class=\"sidebar-header\" style=\"text-align: center;\">HARDWARE STORE</li>
                        <li><a href=\"/main\"><i class=\"fa fa-home\"></i> <span>Home</span></a></li>
                        <li>
                            <a href=\"#\">
                                <i class=\"fa fa-th\"></i> <span>Categories</span> <i class=\"fa fa-angle-left pull-right\"></i>
                            </a>
                            <ul class=\"sidebar-submenu\">
                                ";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categoryList"]) ? $context["categoryList"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 36
            echo "                                    <li><a href=\"/category/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
            echo "\"><i class=\"fa fa-square-o\"></i> ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
            echo "</a></li>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "                            </ul>
                        </li>
                        ";
        // line 40
        if (((isset($context["user"]) ? $context["user"] : null) == "")) {
            // line 41
            echo "                            <li><a href=\"/login\"><i class=\"fa fa-keyboard-o\"></i> <span>Log in</span></a></li>
                            ";
        } else {
            // line 43
            echo "                            <li><a href=\"/logout\"><i class=\"fa fa-keyboard-o\"></i> <span>Log out</span></a></li>
                            ";
        }
        // line 45
        echo "                        <li><a href=\"/cart\"><i class=\"fa fa-opencart\"></i> <span>Cart</span>
                                ";
        // line 46
        if (((isset($context["quan"]) ? $context["quan"] : null) > 0)) {
            // line 47
            echo "                                    <span class=\"label label-primary pull-right\">";
            echo twig_escape_filter($this->env, (isset($context["quan"]) ? $context["quan"] : null), "html", null, true);
            echo "</span>
                                ";
        }
        // line 49
        echo "                            </a>
                        </li>
                    </ul>
                </section>
            </div>
        </div>
        <div class=\"threequarters white\" style=\"min-height: 1080px\">          
            <div class=\"padded\">
                ";
        // line 57
        $this->displayBlock('mainContent', $context, $blocks);
        // line 60
        echo "            </div>
        </div>
    </div>
</body>
</html>";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        echo "Hardware Store";
    }

    // line 19
    public function block_headExtra($context, array $blocks = array())
    {
    }

    // line 57
    public function block_mainContent($context, array $blocks = array())
    {
        // line 58
        echo "                    <p>No content</p>
                ";
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 58,  133 => 57,  128 => 19,  122 => 7,  114 => 60,  112 => 57,  102 => 49,  96 => 47,  94 => 46,  91 => 45,  87 => 43,  83 => 41,  81 => 40,  77 => 38,  66 => 36,  62 => 35,  45 => 20,  43 => 19,  28 => 7,  22 => 3,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# READ ABOUT TEMPLATE INHERITANCE at
    http://twig.sensiolabs.org/doc/2.x/tags/extends.html #}
<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta charset=\"utf-8\">
        <title>{% block title %}Hardware Store{% endblock %}</title>
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/style.css\">
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js\"></script>
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\">
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css\">
        <link rel=\"stylesheet\" href=\"/plugin/menu/dist/sidebar-menu.css\">
        <script src=\"/plugin/menu/dist/sidebar-menu.js\"></script>
        <script>
            \$(document).ready(function () {
                \$.sidebarMenu(\$('.sidebar-menu'));
            });
        </script>
    {% block headExtra %}{% endblock %}
</head>
<body style=\"background: url('http://localhost:8003/bg4.jpg') #E0CCD2 repeat center center fixed;\">

    <div class=\"container black\" style=\"min-height: 1080px\">
        <div class=\"quarter\" style=\"min-height: 1080px\">
            <div class=\"padded\">
                <section class=\"section\">
                    <ul class=\"sidebar-menu\">
                        <li class=\"sidebar-header\" style=\"text-align: center;\">HARDWARE STORE</li>
                        <li><a href=\"/main\"><i class=\"fa fa-home\"></i> <span>Home</span></a></li>
                        <li>
                            <a href=\"#\">
                                <i class=\"fa fa-th\"></i> <span>Categories</span> <i class=\"fa fa-angle-left pull-right\"></i>
                            </a>
                            <ul class=\"sidebar-submenu\">
                                {% for category in categoryList %}
                                    <li><a href=\"/category/{{ category.id }}\"><i class=\"fa fa-square-o\"></i> {{ category.name }}</a></li>
                                    {% endfor %}
                            </ul>
                        </li>
                        {% if user == \"\" %}
                            <li><a href=\"/login\"><i class=\"fa fa-keyboard-o\"></i> <span>Log in</span></a></li>
                            {% else %}
                            <li><a href=\"/logout\"><i class=\"fa fa-keyboard-o\"></i> <span>Log out</span></a></li>
                            {% endif %}
                        <li><a href=\"/cart\"><i class=\"fa fa-opencart\"></i> <span>Cart</span>
                                {% if quan > 0 %}
                                    <span class=\"label label-primary pull-right\">{{ quan }}</span>
                                {% endif %}
                            </a>
                        </li>
                    </ul>
                </section>
            </div>
        </div>
        <div class=\"threequarters white\" style=\"min-height: 1080px\">          
            <div class=\"padded\">
                {% block mainContent %}
                    <p>No content</p>
                {% endblock %}
            </div>
        </div>
    </div>
</body>
</html>", "master.html.twig", "C:\\xampp\\htdocs\\webapps\\hw2eshop\\templates\\master.html.twig");
    }
}
