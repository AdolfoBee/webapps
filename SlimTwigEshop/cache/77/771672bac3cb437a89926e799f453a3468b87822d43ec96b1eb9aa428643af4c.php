<?php

/* category_list.html.twig */
class __TwigTemplate_389e0f829191ef82680f398cf1c4317a0bcce070b511059edfce9f898317c733 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "category_list.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'headExtra' => array($this, 'block_headExtra'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Category List";
    }

    // line 3
    public function block_headExtra($context, array $blocks = array())
    {
        // line 4
        echo "    <script src=\"/plugin/sweetalert2.min.js\"></script>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/plugin/sweetalert2.min.css\">
    <script type=\"text/javascript\">
        \$(document).ready(function() {
            \$('.delete').click(function() {
                \$row=\$(this).attr('categoryID');
                swal({
                    title: 'Are you sure?',
                    text: \"You won't be able to revert this!\",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {
                    swal(
                      'Deleted!',
                      'The category has been deleted.',
                      'success'
                    );
                    \$(location).attr('href', \"/admin/category/delete/\"+\$row);
                });
            });
            
            \$('.edit').click(function(){
                \$(location).attr('href', \"/admin/category/addedit/\"+\$(this).attr('categoryID'));
            });
            
        });
    </script>
";
    }

    // line 35
    public function block_mainContent($context, array $blocks = array())
    {
        // line 36
        echo "    <table>
        <tr>
            <th>ID</th>
            <th>Name</th>
            ";
        // line 40
        if (((isset($context["user"]) ? $context["user"] : null) == "admin")) {
            // line 41
            echo "            <th>Options</th>
            ";
        }
        // line 43
        echo "        </tr>
        ";
        // line 44
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categoryList"]) ? $context["categoryList"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 45
            echo "            <tr>
                <td>";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", array()), "html", null, true);
            echo "</td>
                ";
            // line 48
            if (((isset($context["user"]) ? $context["user"] : null) == "admin")) {
                // line 49
                echo "                <td>
                    <button class=\"edit\" categoryID=\"";
                // line 50
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
                echo "\">Edit</button>
                    <button class=\"delete\" categoryID=\"";
                // line 51
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
                echo "\">Delete</button>
                </td>
                ";
            }
            // line 54
            echo "            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "    </table>
";
    }

    public function getTemplateName()
    {
        return "category_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 56,  121 => 54,  115 => 51,  111 => 50,  108 => 49,  106 => 48,  102 => 47,  98 => 46,  95 => 45,  91 => 44,  88 => 43,  84 => 41,  82 => 40,  76 => 36,  73 => 35,  39 => 4,  36 => 3,  30 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}
{% block title %}Category List{% endblock %}
{% block headExtra %}
    <script src=\"/plugin/sweetalert2.min.js\"></script>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/plugin/sweetalert2.min.css\">
    <script type=\"text/javascript\">
        \$(document).ready(function() {
            \$('.delete').click(function() {
                \$row=\$(this).attr('categoryID');
                swal({
                    title: 'Are you sure?',
                    text: \"You won't be able to revert this!\",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {
                    swal(
                      'Deleted!',
                      'The category has been deleted.',
                      'success'
                    );
                    \$(location).attr('href', \"/admin/category/delete/\"+\$row);
                });
            });
            
            \$('.edit').click(function(){
                \$(location).attr('href', \"/admin/category/addedit/\"+\$(this).attr('categoryID'));
            });
            
        });
    </script>
{% endblock %}
{% block mainContent %}
    <table>
        <tr>
            <th>ID</th>
            <th>Name</th>
            {% if user == \"admin\" %}
            <th>Options</th>
            {% endif %}
        </tr>
        {% for category in categoryList %}
            <tr>
                <td>{{ category.id }}</td>
                <td>{{ category.name }}</td>
                {% if user == \"admin\" %}
                <td>
                    <button class=\"edit\" categoryID=\"{{ category.id }}\">Edit</button>
                    <button class=\"delete\" categoryID=\"{{ category.id }}\">Delete</button>
                </td>
                {% endif %}
            </tr>
        {% endfor %}
    </table>
{% endblock %}", "category_list.html.twig", "C:\\xampp\\htdocs\\webapps\\hw2eshop\\templates\\category_list.html.twig");
    }
}
