<?php

/* main.html.twig */
class __TwigTemplate_813be1fa9a6ecf79a6ff6b86751e8ee25d587f26eb264171ba01652bcfeed442 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "main.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'headExtra' => array($this, 'block_headExtra'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Hardware Store";
    }

    // line 3
    public function block_headExtra($context, array $blocks = array())
    {
    }

    // line 4
    public function block_mainContent($context, array $blocks = array())
    {
        // line 5
        echo "    <div class=\"tags\" style=\"text-align: center;\"><span>HARDWARE STORE</span></div>
    ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["productList"]) ? $context["productList"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 7
            echo "        <div class=\"quarter\" style=\"display: inline-block;\">
          ";
            // line 8
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
            echo "
          <img src=\"/../";
            // line 9
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "image_path", array()), "html", null, true);
            echo "\" alt=\"Hardware Product\">
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 9,  54 => 8,  51 => 7,  47 => 6,  44 => 5,  41 => 4,  36 => 3,  30 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}
{% block title %}Hardware Store{% endblock %}
{% block headExtra %}{% endblock %}
{% block mainContent %}
    <div class=\"tags\" style=\"text-align: center;\"><span>HARDWARE STORE</span></div>
    {% for product in productList %}
        <div class=\"quarter\" style=\"display: inline-block;\">
          {{ product.name }}
          <img src=\"/../{{product.image_path}}\" alt=\"Hardware Product\">
        </div>
    {% endfor %}
{% endblock %}

    ", "main.html.twig", "C:\\xampp\\htdocs\\webapps\\hw2eshop\\templates\\main.html.twig");
    }
}
