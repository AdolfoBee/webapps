<?php

/* category_add_edit.html.twig */
class __TwigTemplate_1b76dd903653ffc7cdf9c45b190f4e029be4aaa5153f4ae7466fc671337bb9bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "category_add_edit.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Add or edit a category";
    }

    // line 5
    public function block_mainContent($context, array $blocks = array())
    {
        // line 6
        echo "    
    ";
        // line 7
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 8
            echo "    <ul>
    ";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errorList"]) ? $context["errorList"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 10
                echo "        <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "    </ul>
    ";
        }
        // line 14
        echo "    
            <form method=\"POST\">
            <div class=\"tags\" style=\"text-align: center;\"><span>CATEGORY</span></div>
            <span> Category Name: <input type=\"text\" name=\"name\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, (isset($context["categoryName"]) ? $context["categoryName"] : null), "html", null, true);
        echo "\"><br></span>
            <input type=\"submit\" value=\"Add / Edit\"><br><br>
            </form>
";
    }

    public function getTemplateName()
    {
        return "category_add_edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 17,  63 => 14,  59 => 12,  50 => 10,  46 => 9,  43 => 8,  41 => 7,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Add or edit a category{% endblock %}

{% block mainContent %}
    
    {% if errorList %}
    <ul>
    {% for error in errorList %}
        <li>{{ error }}</li>
    {% endfor %}
    </ul>
    {% endif %}
    
            <form method=\"POST\">
            <div class=\"tags\" style=\"text-align: center;\"><span>CATEGORY</span></div>
            <span> Category Name: <input type=\"text\" name=\"name\" value=\"{{ categoryName }}\"><br></span>
            <input type=\"submit\" value=\"Add / Edit\"><br><br>
            </form>
{% endblock %}", "category_add_edit.html.twig", "C:\\xampp\\htdocs\\webapps\\hw2eshop\\templates\\category_add_edit.html.twig");
    }
}
