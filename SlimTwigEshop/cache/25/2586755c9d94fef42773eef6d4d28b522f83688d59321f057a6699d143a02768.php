<?php

/* login.html.twig */
class __TwigTemplate_39522fc6f38944eb4c03228f3e6e4e7569d779ed9a68f1d0398c46f5ebce66a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "login.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Log in";
    }

    // line 5
    public function block_mainContent($context, array $blocks = array())
    {
        // line 6
        echo "            <form method=\"POST\">
            <p> Username: <input type=\"text\" name=\"name\" autofocus value=\"";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["username"]) ? $context["username"] : null), "html", null, true);
        echo "\"><br></p>
            <p> Password: <input type=\"password\" name=\"pass\" value=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["password"]) ? $context["password"] : null), "html", null, true);
        echo "\"><br></p>
            <input type=\"submit\" value=\"Login\"><br><br>
            </form>
";
    }

    public function getTemplateName()
    {
        return "login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 8,  41 => 7,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Log in{% endblock %}

{% block mainContent %}
            <form method=\"POST\">
            <p> Username: <input type=\"text\" name=\"name\" autofocus value=\"{{ username }}\"><br></p>
            <p> Password: <input type=\"password\" name=\"pass\" value=\"{{ password }}\"><br></p>
            <input type=\"submit\" value=\"Login\"><br><br>
            </form>
{% endblock %}", "login.html.twig", "C:\\xampp\\htdocs\\webapps\\hw2eshop\\templates\\login.html.twig");
    }
}
