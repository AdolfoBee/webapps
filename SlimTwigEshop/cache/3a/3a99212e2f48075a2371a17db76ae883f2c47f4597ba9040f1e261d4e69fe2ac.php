<?php

/* cart.html.twig */
class __TwigTemplate_609fee8d431b06fd7b0df082c12b1669ee91832dbf09490f87dec1aa7a335177 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "cart.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'headExtra' => array($this, 'block_headExtra'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Cart";
    }

    // line 4
    public function block_headExtra($context, array $blocks = array())
    {
        // line 5
        echo "    <script type=\"text/javascript\">
        \$(document).ready(function() {
            var \$select = \$('.quantity');
            for (i=1;i<=100;i++)
            {
                \$select.append(\$('<option></option>').val(i).html(i));
            }
            
            \$('.quantity').change(function(){
                var q=\$(this);
                \$.ajax(\"/ajax/cart/set/prod/\"+q.attr('cartID')+\"/quantity/\"+q.val())
                .done(function() {
                    alert( \"Success changing the quantity.\" );
                    var total=(q.val()*q.attr('price')).toFixed(2);
                    \$(\"#price[productID='\"+q.attr('productID')+\"']\").text(total+\"\$\");
                })
                .fail(function() {
                  alert( \"There was an error changing the quantity.\" );
                });
            });
            
            \$('.order').click(function(){
                \$(location).attr('href', \"/order\");
            });
            
            \$('.delete').click(function(){
                \$(location).attr('href', \"/cart/delete/\"+\$(this).attr('productID'));
            });
            
        });
    </script>
";
    }

    // line 37
    public function block_mainContent($context, array $blocks = array())
    {
        // line 38
        echo "    ";
        if ((isset($context["productList"]) ? $context["productList"] : null)) {
            // line 39
            echo "        <table>
            <tr>
                ";
            // line 41
            if (((isset($context["user"]) ? $context["user"] : null) == "admin ")) {
                // line 42
                echo "                <th>ID</th>
                ";
            }
            // line 44
            echo "                <th>Name</th>
                <th>Price</th>
                <th>Description</th>
                <th>Image</th>
                ";
            // line 48
            if (((isset($context["user"]) ? $context["user"] : null) == "admin ")) {
                // line 49
                echo "                <th>Options</th>
                ";
            }
            // line 51
            echo "                <th>Quantity</th>
            </tr>
            ";
            // line 53
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["productList"]) ? $context["productList"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 54
                echo "                <tr>
                    ";
                // line 55
                if (((isset($context["user"]) ? $context["user"] : null) == "admin ")) {
                    // line 56
                    echo "                    <td>";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
                    echo "</td>
                    ";
                }
                // line 58
                echo "                    <td><a href=\"/product/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
                echo "</a></td>
                    <td><div productID=\"";
                // line 59
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
                echo "\" id=\"price\">";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($this->getAttribute($context["product"], "price", array()) * $this->getAttribute($context["product"], "quantity", array())), 2, ".", ","), "html", null, true);
                echo "\$</div></td>
                    <td>";
                // line 60
                echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($context["product"], "description", array()), 0, 100), "html", null, true);
                echo "</td>
                    <td><img src=\"/../";
                // line 61
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "image_path", array()), "html", null, true);
                echo "\" alt=\"Hardware Product\" height=\"150\" width=\"150\"></td>
                    <td> 
                        <select class=\"quantity\" productID=\"";
                // line 63
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
                echo "\" price=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "price", array()), "html", null, true);
                echo "\" cartID=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "cartID", array()), "html", null, true);
                echo "\" quantity=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "quantity", array()), "html", null, true);
                echo "\"><option selected>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "quantity", array()), "html", null, true);
                echo "</option></select>
                    </td>
                    <td><button class=\"delete\" productID=\"";
                // line 65
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
                echo "\">Delete</button></td>
                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 68
            echo "        </table>
        <input type=\"submit\" class=\"order\" value=\"Submit an order\">    
    ";
        } else {
            // line 71
            echo "        <span>There are currently no products in the cart.</span>
    ";
        }
    }

    public function getTemplateName()
    {
        return "cart.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 71,  165 => 68,  156 => 65,  143 => 63,  138 => 61,  134 => 60,  128 => 59,  121 => 58,  115 => 56,  113 => 55,  110 => 54,  106 => 53,  102 => 51,  98 => 49,  96 => 48,  90 => 44,  86 => 42,  84 => 41,  80 => 39,  77 => 38,  74 => 37,  39 => 5,  36 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Cart{% endblock %}
{% block headExtra %}
    <script type=\"text/javascript\">
        \$(document).ready(function() {
            var \$select = \$('.quantity');
            for (i=1;i<=100;i++)
            {
                \$select.append(\$('<option></option>').val(i).html(i));
            }
            
            \$('.quantity').change(function(){
                var q=\$(this);
                \$.ajax(\"/ajax/cart/set/prod/\"+q.attr('cartID')+\"/quantity/\"+q.val())
                .done(function() {
                    alert( \"Success changing the quantity.\" );
                    var total=(q.val()*q.attr('price')).toFixed(2);
                    \$(\"#price[productID='\"+q.attr('productID')+\"']\").text(total+\"\$\");
                })
                .fail(function() {
                  alert( \"There was an error changing the quantity.\" );
                });
            });
            
            \$('.order').click(function(){
                \$(location).attr('href', \"/order\");
            });
            
            \$('.delete').click(function(){
                \$(location).attr('href', \"/cart/delete/\"+\$(this).attr('productID'));
            });
            
        });
    </script>
{% endblock %}
{% block mainContent %}
    {% if productList %}
        <table>
            <tr>
                {% if user == \"admin \" %}
                <th>ID</th>
                {% endif %}
                <th>Name</th>
                <th>Price</th>
                <th>Description</th>
                <th>Image</th>
                {% if user == \"admin \" %}
                <th>Options</th>
                {% endif %}
                <th>Quantity</th>
            </tr>
            {% for product in productList %}
                <tr>
                    {% if user == \"admin \" %}
                    <td>{{ product.id }}</td>
                    {% endif %}
                    <td><a href=\"/product/{{product.id}}\">{{ product.name }}</a></td>
                    <td><div productID=\"{{product.id}}\" id=\"price\">{{ (product.price * product.quantity)|number_format(2, '.', ',') }}\$</div></td>
                    <td>{{ product.description|slice(0, 100) }}</td>
                    <td><img src=\"/../{{product.image_path}}\" alt=\"Hardware Product\" height=\"150\" width=\"150\"></td>
                    <td> 
                        <select class=\"quantity\" productID=\"{{product.id}}\" price=\"{{ product.price }}\" cartID=\"{{ product.cartID }}\" quantity=\"{{ product.quantity }}\"><option selected>{{ product.quantity }}</option></select>
                    </td>
                    <td><button class=\"delete\" productID=\"{{ product.id }}\">Delete</button></td>
                </tr>
            {% endfor %}
        </table>
        <input type=\"submit\" class=\"order\" value=\"Submit an order\">    
    {% else %}
        <span>There are currently no products in the cart.</span>
    {% endif %}
{% endblock %}", "cart.html.twig", "C:\\xampp\\htdocs\\webapps\\hw2eshop\\templates\\cart.html.twig");
    }
}
