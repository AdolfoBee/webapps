<?php

/* order.html.twig */
class __TwigTemplate_9b3f58ec3c101f0d3850b6db0fa9f7a8f305309832611c7489513f5d2c674f7a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "order.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Order";
    }

    // line 5
    public function block_mainContent($context, array $blocks = array())
    {
        // line 6
        echo "  
    ";
        // line 7
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 8
            echo "    <ul>
    ";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errorList"]) ? $context["errorList"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 10
                echo "        <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "    </ul>
    ";
        }
        // line 14
        echo "    
            <form method=\"POST\" enctype=\"multipart/form-data\">
            <div class=\"tags\" style=\"text-align: center;\"><span>ORDER</span></div>
            <span> First Name: <input type=\"text\" name=\"firstName\" autofocus value=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "firstName", array()), "html", null, true);
        echo "\"><br></span>
            <span> Last Name: <input type=\"text\" name=\"lastName\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "lastName", array()), "html", null, true);
        echo "\"><br></span>
            <span> Address: <input type=\"text\" name=\"address\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "address", array()), "html", null, true);
        echo "\"><br></span>
            <span> Post Code: <input type=\"text\" name=\"postCode\" value=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "postCode", array()), "html", null, true);
        echo "\"><br></span>
            <span> Country: <input type=\"text\" name=\"country\" value=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "country", array()), "html", null, true);
        echo "\"><br></span>
            <span> Province / State: <input type=\"text\" name=\"province\" value=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "province", array()), "html", null, true);
        echo "\"><br></span>
            <span> Email: <input type=\"text\" name=\"email\" value=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "email", array()), "html", null, true);
        echo "\"><br></span>
            <span> Confirm Email: <input type=\"text\" name=\"cemail\" value=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "cemail", array()), "html", null, true);
        echo "\"><br></span>
            <span> Phone: <input type=\"text\" name=\"phone\" value=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "phone", array()), "html", null, true);
        echo "\"><br></span>
            <span> Credit Card No: <input type=\"text\" name=\"cnum\" value=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "cnum", array()), "html", null, true);
        echo "\"><br></span>
            <span> Credit Card Expirity: <input type=\"date\" name=\"cex\" value=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "cex", array()), "html", null, true);
        echo "\"><br></span>
            <span> Credit Card CVV: <input type=\"text\" name=\"ccvv\" value=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "ccvv", array()), "html", null, true);
        echo "\"><br></span>
            <span> Total before taxes and delivery: <input type=\"text\" name=\"pretotal\" value=\"";
        // line 29
        echo twig_escape_filter($this->env, (isset($context["pretotal"]) ? $context["pretotal"] : null), "html", null, true);
        echo "\" readonly><br></span>
            <span> Delivery: <input type=\"text\" name=\"delivery\" value=\"15\" readonly><br></span>
            <span> Taxes: <input type=\"text\" name=\"taxes\" value=\"12%\" readonly><br></span>
            <span> Total: <input type=\"text\" name=\"total\" value=\"";
        // line 32
        echo twig_escape_filter($this->env, (isset($context["total"]) ? $context["total"] : null), "html", null, true);
        echo "\" readonly><br></span>
            
            <input type=\"submit\" value=\"Submit the order\"><br><br>
            </form>
";
    }

    public function getTemplateName()
    {
        return "order.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 32,  116 => 29,  112 => 28,  108 => 27,  104 => 26,  100 => 25,  96 => 24,  92 => 23,  88 => 22,  84 => 21,  80 => 20,  76 => 19,  72 => 18,  68 => 17,  63 => 14,  59 => 12,  50 => 10,  46 => 9,  43 => 8,  41 => 7,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Order{% endblock %}

{% block mainContent %}
  
    {% if errorList %}
    <ul>
    {% for error in errorList %}
        <li>{{ error }}</li>
    {% endfor %}
    </ul>
    {% endif %}
    
            <form method=\"POST\" enctype=\"multipart/form-data\">
            <div class=\"tags\" style=\"text-align: center;\"><span>ORDER</span></div>
            <span> First Name: <input type=\"text\" name=\"firstName\" autofocus value=\"{{ v.firstName }}\"><br></span>
            <span> Last Name: <input type=\"text\" name=\"lastName\" value=\"{{ v.lastName }}\"><br></span>
            <span> Address: <input type=\"text\" name=\"address\" value=\"{{ v.address }}\"><br></span>
            <span> Post Code: <input type=\"text\" name=\"postCode\" value=\"{{ v.postCode }}\"><br></span>
            <span> Country: <input type=\"text\" name=\"country\" value=\"{{ v.country }}\"><br></span>
            <span> Province / State: <input type=\"text\" name=\"province\" value=\"{{ v.province }}\"><br></span>
            <span> Email: <input type=\"text\" name=\"email\" value=\"{{ v.email }}\"><br></span>
            <span> Confirm Email: <input type=\"text\" name=\"cemail\" value=\"{{ v.cemail }}\"><br></span>
            <span> Phone: <input type=\"text\" name=\"phone\" value=\"{{ v.phone }}\"><br></span>
            <span> Credit Card No: <input type=\"text\" name=\"cnum\" value=\"{{ v.cnum }}\"><br></span>
            <span> Credit Card Expirity: <input type=\"date\" name=\"cex\" value=\"{{ v.cex }}\"><br></span>
            <span> Credit Card CVV: <input type=\"text\" name=\"ccvv\" value=\"{{ v.ccvv }}\"><br></span>
            <span> Total before taxes and delivery: <input type=\"text\" name=\"pretotal\" value=\"{{ pretotal }}\" readonly><br></span>
            <span> Delivery: <input type=\"text\" name=\"delivery\" value=\"15\" readonly><br></span>
            <span> Taxes: <input type=\"text\" name=\"taxes\" value=\"12%\" readonly><br></span>
            <span> Total: <input type=\"text\" name=\"total\" value=\"{{ total }}\" readonly><br></span>
            
            <input type=\"submit\" value=\"Submit the order\"><br><br>
            </form>
{% endblock %}", "order.html.twig", "C:\\xampp\\htdocs\\webapps\\hw2eshop\\templates\\order.html.twig");
    }
}
