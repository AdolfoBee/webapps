<?php

/* product.html.twig */
class __TwigTemplate_65292ae56df43ded0caa22152fce38c984e93e4798263774e9c06e002546aaa2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "product.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'headExtra' => array($this, 'block_headExtra'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Product List";
    }

    // line 4
    public function block_headExtra($context, array $blocks = array())
    {
        // line 5
        echo "    <script src=\"/plugin/sweetalert2.min.js\"></script>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/plugin/sweetalert2.min.css\">
    <script type=\"text/javascript\">
        \$(document).ready(function() {
            \$('.delete').click(function() {
                \$row=\$(this).attr('productID');
                swal({
                    title: 'Are you sure?',
                    text: \"You won't be able to revert this!\",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {
                    swal(
                      'Deleted!',
                      'The product has been deleted.',
                      'success'
                    );
                    \$(location).attr('href', \"/admin/product/delete/\"+\$row);
                });
            });
            
            \$('.edit').click(function(){
                \$(location).attr('href', \"/admin/product/addedit/\"+\$(this).attr('productID'));
            });
            
            \$('.addToCart').click(function(){
                \$(location).attr('href', \"/cart/add/\"+\$(this).attr('productID'));
            });
        });
    </script>
";
    }

    // line 39
    public function block_mainContent($context, array $blocks = array())
    {
        // line 40
        echo "    <table>
        <tr>
            ";
        // line 42
        if (((isset($context["user"]) ? $context["user"] : null) == "admin")) {
            // line 43
            echo "            <th>ID</th>
            ";
        }
        // line 45
        echo "            <th>Name</th>
            <th>Price</th>
            <th>Description</th>
            <th>Image</th>
            ";
        // line 49
        if (((isset($context["user"]) ? $context["user"] : null) == "admin")) {
            // line 50
            echo "            <th>Options</th>
            ";
        }
        // line 52
        echo "        </tr>
        ";
        // line 53
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["productList"]) ? $context["productList"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 54
            echo "            <tr>
                ";
            // line 55
            if (((isset($context["user"]) ? $context["user"] : null) == "admin")) {
                // line 56
                echo "                <td>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
                echo "</td>
                ";
            }
            // line 58
            echo "                <td><a href=\"/product/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 59
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "price", array()), "html", null, true);
            echo "\$</td>
                <td>";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "description", array()), "html", null, true);
            echo "</td>
                <td><img src=\"/../";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "image_path", array()), "html", null, true);
            echo "\" alt=\"Hardware Product\" height=\"350\" width=\"350\"></td>
                ";
            // line 62
            if (((isset($context["user"]) ? $context["user"] : null) == "admin")) {
                // line 63
                echo "                <td>
                    <button class=\"edit\" productID=\"";
                // line 64
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
                echo "\">Edit</button>
                    <button class=\"delete\" productID=\"";
                // line 65
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
                echo "\">Delete</button>
                </td>
                ";
            }
            // line 68
            echo "                <td><button class=\"addToCart\" productID=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "\">Add to Cart</button></td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 71
        echo "    </table>
";
    }

    public function getTemplateName()
    {
        return "product.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 71,  153 => 68,  147 => 65,  143 => 64,  140 => 63,  138 => 62,  134 => 61,  130 => 60,  126 => 59,  119 => 58,  113 => 56,  111 => 55,  108 => 54,  104 => 53,  101 => 52,  97 => 50,  95 => 49,  89 => 45,  85 => 43,  83 => 42,  79 => 40,  76 => 39,  39 => 5,  36 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Product List{% endblock %}
{% block headExtra %}
    <script src=\"/plugin/sweetalert2.min.js\"></script>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/plugin/sweetalert2.min.css\">
    <script type=\"text/javascript\">
        \$(document).ready(function() {
            \$('.delete').click(function() {
                \$row=\$(this).attr('productID');
                swal({
                    title: 'Are you sure?',
                    text: \"You won't be able to revert this!\",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {
                    swal(
                      'Deleted!',
                      'The product has been deleted.',
                      'success'
                    );
                    \$(location).attr('href', \"/admin/product/delete/\"+\$row);
                });
            });
            
            \$('.edit').click(function(){
                \$(location).attr('href', \"/admin/product/addedit/\"+\$(this).attr('productID'));
            });
            
            \$('.addToCart').click(function(){
                \$(location).attr('href', \"/cart/add/\"+\$(this).attr('productID'));
            });
        });
    </script>
{% endblock %}
{% block mainContent %}
    <table>
        <tr>
            {% if user == \"admin\" %}
            <th>ID</th>
            {% endif %}
            <th>Name</th>
            <th>Price</th>
            <th>Description</th>
            <th>Image</th>
            {% if user == \"admin\" %}
            <th>Options</th>
            {% endif %}
        </tr>
        {% for product in productList %}
            <tr>
                {% if user == \"admin\" %}
                <td>{{ product.id }}</td>
                {% endif %}
                <td><a href=\"/product/{{product.id}}\">{{ product.name }}</a></td>
                <td>{{ product.price }}\$</td>
                <td>{{ product.description }}</td>
                <td><img src=\"/../{{product.image_path}}\" alt=\"Hardware Product\" height=\"350\" width=\"350\"></td>
                {% if user == \"admin\" %}
                <td>
                    <button class=\"edit\" productID=\"{{ product.id }}\">Edit</button>
                    <button class=\"delete\" productID=\"{{ product.id }}\">Delete</button>
                </td>
                {% endif %}
                <td><button class=\"addToCart\" productID=\"{{ product.id }}\">Add to Cart</button></td>
            </tr>
        {% endfor %}
    </table>
{% endblock %}", "product.html.twig", "C:\\xampp\\htdocs\\webapps\\hw2eshop\\templates\\product.html.twig");
    }
}
