<?php

/* category_add_edit_success.html.twig */
class __TwigTemplate_8ed4be98e814039f0e8ff16ed2383735f89c1e06a01bc36736ef57e14b3c6799 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["addedit"]) ? $context["addedit"] : null) == 0)) {
            // line 2
            echo "    <p>Category added.</p>
";
        } else {
            // line 4
            echo "    <p>Category edited.</p>
";
        }
        // line 6
        echo "
";
    }

    public function getTemplateName()
    {
        return "category_add_edit_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 6,  25 => 4,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if addedit ==0 %}
    <p>Category added.</p>
{% else %}
    <p>Category edited.</p>
{% endif %}

", "category_add_edit_success.html.twig", "C:\\xampp\\htdocs\\webapps\\hw2eshop\\templates\\category_add_edit_success.html.twig");
    }
}
