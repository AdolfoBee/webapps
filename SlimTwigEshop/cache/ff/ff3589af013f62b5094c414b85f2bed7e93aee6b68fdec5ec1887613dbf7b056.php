<?php

/* error_internal.html.twig */
class __TwigTemplate_d48d8de3ae18e08915774ea1a7f38dd056e9a67a3848feb53888707f67555a2c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "error_internal.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Internal error";
    }

    // line 5
    public function block_mainContent($context, array $blocks = array())
    {
        // line 6
        echo "    <h1>Internal error</h1>
    <!--<img src=\"/media/ninja.png\" width=\"150\">-->
    <p>We're very sorry, we failed to fulfill your request.
        Our team of coding black ninjas has already
        been notified of your trouble.</p>
    <p><a href=\"/\">Click here</a> to continue</p>
    
    
";
    }

    public function getTemplateName()
    {
        return "error_internal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Internal error{% endblock %}

{% block mainContent %}
    <h1>Internal error</h1>
    <!--<img src=\"/media/ninja.png\" width=\"150\">-->
    <p>We're very sorry, we failed to fulfill your request.
        Our team of coding black ninjas has already
        been notified of your trouble.</p>
    <p><a href=\"/\">Click here</a> to continue</p>
    
    
{% endblock %}", "error_internal.html.twig", "C:\\xampp\\htdocs\\webapps\\hw2eshop\\templates\\error_internal.html.twig");
    }
}
