<?php

/* product_add_edit.html.twig */
class __TwigTemplate_0ac221e8de60c9b13c3d97fe2958173b6be4958c06b160b9d9e8deadbfb33cd8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "product_add_edit.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Add or edit a product";
    }

    // line 5
    public function block_mainContent($context, array $blocks = array())
    {
        // line 6
        echo "  
    ";
        // line 7
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 8
            echo "    <ul>
    ";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errorList"]) ? $context["errorList"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 10
                echo "        <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "    </ul>
    ";
        }
        // line 14
        echo "    
            <form method=\"POST\" enctype=\"multipart/form-data\">
            <div class=\"tags\" style=\"text-align: center;\"><span>PRODUCT</span></div>
            <span> Name: <input type=\"text\" name=\"name\" autofocus value=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "name", array()), "html", null, true);
        echo "\"><br></span>
            <span> Description: </span><textarea name=\"description\" style=\"display: inline-block;\">";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "description", array()), "html", null, true);
        echo "</textarea>
            <span> Price: <input type=\"number\" name=\"price\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "price", array()), "html", null, true);
        echo "\" min=\"1\" step=\"any\"><br></span>
            <span> Category:<select name=\"category\">
            <option value=\"none\"> ---- </option>
            ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categoryNames"]) ? $context["categoryNames"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 23
            echo "                ";
            if (($context["category"] == $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "categoryName", array()))) {
                // line 24
                echo "                    <option selected=\"selected\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "categoryName", array()), "html", null, true);
                echo "\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "categoryName", array()), "html", null, true);
                echo "</option>
                ";
            } else {
                // line 26
                echo "                    <option value=\"";
                echo twig_escape_filter($this->env, $context["category"], "html", null, true);
                echo "\"> ";
                echo twig_escape_filter($this->env, $context["category"], "html", null, true);
                echo "</option>
                ";
            }
            // line 28
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "            </select></span>
            <span> Image: <input type=\"file\" name=\"imageUpload\"><br></span>
            <span> Front Page:<br>
                <input type=\"radio\" name=\"fp\" value=\"no\" checked> No<br>
                <input type=\"radio\" name=\"fp\" value=\"yes\"> Yes
            </span>
            <input type=\"submit\" value=\"Add / Edit\"><br><br>
            </form>
";
    }

    public function getTemplateName()
    {
        return "product_add_edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 29,  105 => 28,  97 => 26,  89 => 24,  86 => 23,  82 => 22,  76 => 19,  72 => 18,  68 => 17,  63 => 14,  59 => 12,  50 => 10,  46 => 9,  43 => 8,  41 => 7,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Add or edit a product{% endblock %}

{% block mainContent %}
  
    {% if errorList %}
    <ul>
    {% for error in errorList %}
        <li>{{ error }}</li>
    {% endfor %}
    </ul>
    {% endif %}
    
            <form method=\"POST\" enctype=\"multipart/form-data\">
            <div class=\"tags\" style=\"text-align: center;\"><span>PRODUCT</span></div>
            <span> Name: <input type=\"text\" name=\"name\" autofocus value=\"{{ v.name }}\"><br></span>
            <span> Description: </span><textarea name=\"description\" style=\"display: inline-block;\">{{ v.description }}</textarea>
            <span> Price: <input type=\"number\" name=\"price\" value=\"{{ v.price }}\" min=\"1\" step=\"any\"><br></span>
            <span> Category:<select name=\"category\">
            <option value=\"none\"> ---- </option>
            {% for category in categoryNames %}
                {% if category == v.categoryName %}
                    <option selected=\"selected\" value=\"{{ v.categoryName }}\"> {{ v.categoryName }}</option>
                {% else %}
                    <option value=\"{{ category }}\"> {{ category }}</option>
                {% endif %}
            {% endfor %}
            </select></span>
            <span> Image: <input type=\"file\" name=\"imageUpload\"><br></span>
            <span> Front Page:<br>
                <input type=\"radio\" name=\"fp\" value=\"no\" checked> No<br>
                <input type=\"radio\" name=\"fp\" value=\"yes\"> Yes
            </span>
            <input type=\"submit\" value=\"Add / Edit\"><br><br>
            </form>
{% endblock %}", "product_add_edit.html.twig", "C:\\xampp\\htdocs\\webapps\\hw2eshop\\templates\\product_add_edit.html.twig");
    }
}
