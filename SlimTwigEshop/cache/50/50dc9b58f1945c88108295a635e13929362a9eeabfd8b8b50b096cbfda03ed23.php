<?php

/* product_list.html.twig */
class __TwigTemplate_d38e9f1e06e35c19f6cb798fdf89031825c61dba3d6e6ab9074098e04182b356 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "product_list.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'headExtra' => array($this, 'block_headExtra'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Product List";
    }

    // line 3
    public function block_headExtra($context, array $blocks = array())
    {
        // line 4
        echo "    <script src=\"/plugin/sweetalert2.min.js\"></script>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/plugin/sweetalert2.min.css\">
    <script type=\"text/javascript\">
        \$(document).ready(function() {
            \$('.delete').click(function() {
                \$row=\$(this).attr('productID');
                swal({
                    title: 'Are you sure?',
                    text: \"You won't be able to revert this!\",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {
                    swal(
                      'Deleted!',
                      'The product has been deleted.',
                      'success'
                    );
                    \$(location).attr('href', \"/admin/product/delete/\"+\$row);
                });
            });
            
            \$('.edit').click(function(){
                \$(location).attr('href', \"/admin/product/addedit/\"+\$(this).attr('productID'));
            });
            
            \$('.addToCart').click(function(){
                \$.ajax(\"/cart/add/\"+\$(this).attr('productID'));
                \$(this).attr(\"disabled\",\"disabled\");
                \$(this).text('Added to Cart');
            });
            /*
            \$('.addToCart').click(function(){
                var \$button = \$(this);
                \$button.attr(\"disabled\",\"disabled\");
                \$.ajax({
                    url: \"/cart/add/\"+\$button.attr('productID'),
                    success: function (data) {
                         \$button.text('Added to Cart');
                    }
                });
            });*/
        });
    </script>
";
    }

    // line 51
    public function block_mainContent($context, array $blocks = array())
    {
        // line 52
        echo "    <table>
        <tr>
            ";
        // line 54
        if (((isset($context["user"]) ? $context["user"] : null) == "admin")) {
            // line 55
            echo "            <th>ID</th>
            ";
        }
        // line 57
        echo "            <th>Name</th>
            <th>Price</th>
            <th>Description</th>
            <th>Image</th>
            ";
        // line 61
        if (((isset($context["user"]) ? $context["user"] : null) == "admin")) {
            // line 62
            echo "            <th>Options</th>
            ";
        }
        // line 64
        echo "        </tr>
        ";
        // line 65
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["productList"]) ? $context["productList"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 66
            echo "            <tr>
                ";
            // line 67
            if (((isset($context["user"]) ? $context["user"] : null) == "admin")) {
                // line 68
                echo "                <td>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
                echo "</td>
                ";
            }
            // line 70
            echo "                <td><a href=\"/product/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "price", array()), "html", null, true);
            echo "\$</td>
                <td>";
            // line 72
            echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($context["product"], "description", array()), 0, 100), "html", null, true);
            echo "</td>
                <td><img src=\"/../";
            // line 73
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "image_path", array()), "html", null, true);
            echo "\" alt=\"Hardware Product\" height=\"150\" width=\"150\"></td>
                ";
            // line 74
            if (((isset($context["user"]) ? $context["user"] : null) == "admin")) {
                // line 75
                echo "                <td>
                    <button class=\"edit\" productID=\"";
                // line 76
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
                echo "\">Edit</button>
                    <button class=\"delete\" productID=\"";
                // line 77
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
                echo "\">Delete</button>
                </td>
                ";
            }
            // line 80
            echo "                ";
            if (((isset($context["user"]) ? $context["user"] : null) != "admin")) {
                // line 81
                echo "                    ";
                if ((isset($context["productList2"]) ? $context["productList2"] : null)) {
                    // line 82
                    echo "                        ";
                    $context["isThere"] = "no";
                    // line 83
                    echo "                        
                        ";
                    // line 84
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["productList2"]) ? $context["productList2"] : null));
                    foreach ($context['_seq'] as $context["_key"] => $context["product2"]) {
                        // line 85
                        echo "                            ";
                        if (($this->getAttribute($context["product"], "id", array()) == $this->getAttribute($context["product2"], "productID", array()))) {
                            // line 86
                            echo "                                ";
                            $context["isThere"] = "yes";
                            // line 87
                            echo "                            ";
                        }
                        // line 88
                        echo "                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product2'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 89
                    echo "                        
                        ";
                    // line 90
                    if (((isset($context["isThere"]) ? $context["isThere"] : null) == "yes")) {
                        // line 91
                        echo "                            <td><button class=\"addToCart\" productID=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
                        echo "\" disabled>Added to Cart</button></td>
                        ";
                    } else {
                        // line 93
                        echo "                            <td><button class=\"addToCart\" productID=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
                        echo "\">Add to Cart</button></td>
                        ";
                    }
                    // line 95
                    echo "                    ";
                } else {
                    // line 96
                    echo "                        <td><button class=\"addToCart\" productID=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
                    echo "\">Add to Cart</button></td>
                    ";
                }
                // line 98
                echo "                ";
            }
            // line 99
            echo "            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 101
        echo "    </table>
";
    }

    public function getTemplateName()
    {
        return "product_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  233 => 101,  226 => 99,  223 => 98,  217 => 96,  214 => 95,  208 => 93,  202 => 91,  200 => 90,  197 => 89,  191 => 88,  188 => 87,  185 => 86,  182 => 85,  178 => 84,  175 => 83,  172 => 82,  169 => 81,  166 => 80,  160 => 77,  156 => 76,  153 => 75,  151 => 74,  147 => 73,  143 => 72,  139 => 71,  132 => 70,  126 => 68,  124 => 67,  121 => 66,  117 => 65,  114 => 64,  110 => 62,  108 => 61,  102 => 57,  98 => 55,  96 => 54,  92 => 52,  89 => 51,  39 => 4,  36 => 3,  30 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}
{% block title %}Product List{% endblock %}
{% block headExtra %}
    <script src=\"/plugin/sweetalert2.min.js\"></script>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/plugin/sweetalert2.min.css\">
    <script type=\"text/javascript\">
        \$(document).ready(function() {
            \$('.delete').click(function() {
                \$row=\$(this).attr('productID');
                swal({
                    title: 'Are you sure?',
                    text: \"You won't be able to revert this!\",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {
                    swal(
                      'Deleted!',
                      'The product has been deleted.',
                      'success'
                    );
                    \$(location).attr('href', \"/admin/product/delete/\"+\$row);
                });
            });
            
            \$('.edit').click(function(){
                \$(location).attr('href', \"/admin/product/addedit/\"+\$(this).attr('productID'));
            });
            
            \$('.addToCart').click(function(){
                \$.ajax(\"/cart/add/\"+\$(this).attr('productID'));
                \$(this).attr(\"disabled\",\"disabled\");
                \$(this).text('Added to Cart');
            });
            /*
            \$('.addToCart').click(function(){
                var \$button = \$(this);
                \$button.attr(\"disabled\",\"disabled\");
                \$.ajax({
                    url: \"/cart/add/\"+\$button.attr('productID'),
                    success: function (data) {
                         \$button.text('Added to Cart');
                    }
                });
            });*/
        });
    </script>
{% endblock %}
{% block mainContent %}
    <table>
        <tr>
            {% if user == \"admin\" %}
            <th>ID</th>
            {% endif %}
            <th>Name</th>
            <th>Price</th>
            <th>Description</th>
            <th>Image</th>
            {% if user == \"admin\" %}
            <th>Options</th>
            {% endif %}
        </tr>
        {% for product in productList %}
            <tr>
                {% if user == \"admin\" %}
                <td>{{ product.id }}</td>
                {% endif %}
                <td><a href=\"/product/{{product.id}}\">{{ product.name }}</a></td>
                <td>{{ product.price }}\$</td>
                <td>{{ product.description|slice(0, 100) }}</td>
                <td><img src=\"/../{{product.image_path}}\" alt=\"Hardware Product\" height=\"150\" width=\"150\"></td>
                {% if user == \"admin\" %}
                <td>
                    <button class=\"edit\" productID=\"{{ product.id }}\">Edit</button>
                    <button class=\"delete\" productID=\"{{ product.id }}\">Delete</button>
                </td>
                {% endif %}
                {% if user != \"admin\" %}
                    {% if productList2 %}
                        {% set isThere = 'no' %}
                        
                        {% for product2 in productList2 %}
                            {% if product.id == product2.productID %}
                                {% set isThere = 'yes' %}
                            {% endif %}
                        {% endfor %}
                        
                        {% if isThere =='yes' %}
                            <td><button class=\"addToCart\" productID=\"{{ product.id }}\" disabled>Added to Cart</button></td>
                        {% else %}
                            <td><button class=\"addToCart\" productID=\"{{ product.id }}\">Add to Cart</button></td>
                        {% endif %}
                    {% else %}
                        <td><button class=\"addToCart\" productID=\"{{ product.id }}\">Add to Cart</button></td>
                    {% endif %}
                {% endif %}
            </tr>
        {% endfor %}
    </table>
{% endblock %}", "product_list.html.twig", "C:\\xampp\\htdocs\\webapps\\hw2eshop\\templates\\product_list.html.twig");
    }
}
