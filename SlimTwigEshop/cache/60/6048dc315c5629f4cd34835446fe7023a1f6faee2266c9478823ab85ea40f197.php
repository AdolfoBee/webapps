<?php

/* product_lists.html.twig */
class __TwigTemplate_6455b7101de83c5bb8e58a9c304d1c2389186a2687d1d441a807c95078f28afb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "product_lists.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'headextra' => array($this, 'block_headextra'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Product List";
    }

    // line 4
    public function block_headextra($context, array $blocks = array())
    {
        // line 5
        echo "    <script src=\"/plugin/sweetalert2.min.js\"></script>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/plugin/sweetalert2.min.css\">
    <script type=\"text/javascript\">
        \$(document).ready(function() {
            \$('.delete').click(function() {
                \$row=\$(this).attr('productID');
                swal({
                    title: 'Are you sure?',
                    text: \"You won't be able to revert this!\",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {
                    swal(
                      'Deleted!',
                      'The product has been deleted.',
                      'success'
                    );
                    \$(location).attr('href', \"/admin/product/delete/\"+\$row);
                });
            });
            
            \$('.edit').click(function(){
                \$(location).attr('href', \"/admin/product/addedit/\"+\$(this).attr('productID'));
            });
            
            \$('.addToCart').click(function(){
                \$.ajax(\"/cart/add/\"+\$(this).attr('productID'));
                \$(this).attr(\"disabled\",\"disabled\");
                \$(this).text('Added to Cart');
            });
        });
    </script>
";
    }

    // line 41
    public function block_mainContent($context, array $blocks = array())
    {
        // line 42
        echo "    <table>
        <tr>
            ";
        // line 44
        if (((isset($context["user"]) ? $context["user"] : null) == "admin")) {
            // line 45
            echo "            <th>ID</th>
            ";
        }
        // line 47
        echo "            <th>Name</th>
            <th>Price</th>
            <th>Description</th>
            <th>Image</th>
            ";
        // line 51
        if (((isset($context["user"]) ? $context["user"] : null) == "admin")) {
            // line 52
            echo "            <th>Options</th>
            ";
        }
        // line 54
        echo "        </tr>
        ";
        // line 55
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["productList"]) ? $context["productList"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 56
            echo "            <tr>
                ";
            // line 57
            if (((isset($context["user"]) ? $context["user"] : null) == "admin")) {
                // line 58
                echo "                <td>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
                echo "</td>
                ";
            }
            // line 60
            echo "                <td><a href=\"/product/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "price", array()), "html", null, true);
            echo "\$</td>
                <td>";
            // line 62
            echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($context["product"], "description", array()), 0, 100), "html", null, true);
            echo "</td>
                <td><img src=\"/../";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "image_path", array()), "html", null, true);
            echo "\" alt=\"Hardware Product\" height=\"150\" width=\"150\"></td>
                ";
            // line 64
            if (((isset($context["user"]) ? $context["user"] : null) == "admin")) {
                // line 65
                echo "                <td>
                    <button class=\"edit\" productID=\"";
                // line 66
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
                echo "\">Edit</button>
                    <button class=\"delete\" productID=\"";
                // line 67
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
                echo "\">Delete</button>
                </td>
                ";
            }
            // line 70
            echo "                ";
            if (((isset($context["user"]) ? $context["user"] : null) != "admin")) {
                // line 71
                echo "                    ";
                if ((isset($context["productList2"]) ? $context["productList2"] : null)) {
                    // line 72
                    echo "                        ";
                    $context["isThere"] = "no";
                    // line 73
                    echo "                        
                        ";
                    // line 74
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["productList2"]) ? $context["productList2"] : null));
                    foreach ($context['_seq'] as $context["_key"] => $context["product2"]) {
                        // line 75
                        echo "                            ";
                        if (($this->getAttribute($context["product"], "id", array()) == $this->getAttribute($context["product2"], "productID", array()))) {
                            // line 76
                            echo "                                ";
                            $context["isThere"] = "yes";
                            // line 77
                            echo "                            ";
                        }
                        // line 78
                        echo "                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product2'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 79
                    echo "                        
                        ";
                    // line 80
                    if (((isset($context["isThere"]) ? $context["isThere"] : null) == "yes")) {
                        // line 81
                        echo "                            <td><button class=\"addToCart\" productID=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
                        echo "\" disabled>Added to Cart</button></td>
                        ";
                    } else {
                        // line 83
                        echo "                            <td><button class=\"addToCart\" productID=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
                        echo "\">Add to Cart</button></td>
                        ";
                    }
                    // line 85
                    echo "                    ";
                } else {
                    // line 86
                    echo "                        <td><button class=\"addToCart\" productID=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
                    echo "\">Add to Cart</button></td>
                    ";
                }
                // line 88
                echo "                ";
            }
            // line 89
            echo "            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 91
        echo "    </table>
";
    }

    public function getTemplateName()
    {
        return "product_lists.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  222 => 91,  215 => 89,  212 => 88,  206 => 86,  203 => 85,  197 => 83,  191 => 81,  189 => 80,  186 => 79,  180 => 78,  177 => 77,  174 => 76,  171 => 75,  167 => 74,  164 => 73,  161 => 72,  158 => 71,  155 => 70,  149 => 67,  145 => 66,  142 => 65,  140 => 64,  136 => 63,  132 => 62,  128 => 61,  121 => 60,  115 => 58,  113 => 57,  110 => 56,  106 => 55,  103 => 54,  99 => 52,  97 => 51,  91 => 47,  87 => 45,  85 => 44,  81 => 42,  78 => 41,  39 => 5,  36 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Product List{% endblock %}
{% block headextra %}
    <script src=\"/plugin/sweetalert2.min.js\"></script>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/plugin/sweetalert2.min.css\">
    <script type=\"text/javascript\">
        \$(document).ready(function() {
            \$('.delete').click(function() {
                \$row=\$(this).attr('productID');
                swal({
                    title: 'Are you sure?',
                    text: \"You won't be able to revert this!\",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {
                    swal(
                      'Deleted!',
                      'The product has been deleted.',
                      'success'
                    );
                    \$(location).attr('href', \"/admin/product/delete/\"+\$row);
                });
            });
            
            \$('.edit').click(function(){
                \$(location).attr('href', \"/admin/product/addedit/\"+\$(this).attr('productID'));
            });
            
            \$('.addToCart').click(function(){
                \$.ajax(\"/cart/add/\"+\$(this).attr('productID'));
                \$(this).attr(\"disabled\",\"disabled\");
                \$(this).text('Added to Cart');
            });
        });
    </script>
{% endblock %}
{% block mainContent %}
    <table>
        <tr>
            {% if user == \"admin\" %}
            <th>ID</th>
            {% endif %}
            <th>Name</th>
            <th>Price</th>
            <th>Description</th>
            <th>Image</th>
            {% if user == \"admin\" %}
            <th>Options</th>
            {% endif %}
        </tr>
        {% for product in productList %}
            <tr>
                {% if user == \"admin\" %}
                <td>{{ product.id }}</td>
                {% endif %}
                <td><a href=\"/product/{{product.id}}\">{{ product.name }}</a></td>
                <td>{{ product.price }}\$</td>
                <td>{{ product.description|slice(0, 100) }}</td>
                <td><img src=\"/../{{product.image_path}}\" alt=\"Hardware Product\" height=\"150\" width=\"150\"></td>
                {% if user == \"admin\" %}
                <td>
                    <button class=\"edit\" productID=\"{{ product.id }}\">Edit</button>
                    <button class=\"delete\" productID=\"{{ product.id }}\">Delete</button>
                </td>
                {% endif %}
                {% if user != \"admin\" %}
                    {% if productList2 %}
                        {% set isThere = 'no' %}
                        
                        {% for product2 in productList2 %}
                            {% if product.id == product2.productID %}
                                {% set isThere = 'yes' %}
                            {% endif %}
                        {% endfor %}
                        
                        {% if isThere =='yes' %}
                            <td><button class=\"addToCart\" productID=\"{{ product.id }}\" disabled>Added to Cart</button></td>
                        {% else %}
                            <td><button class=\"addToCart\" productID=\"{{ product.id }}\">Add to Cart</button></td>
                        {% endif %}
                    {% else %}
                        <td><button class=\"addToCart\" productID=\"{{ product.id }}\">Add to Cart</button></td>
                    {% endif %}
                {% endif %}
            </tr>
        {% endfor %}
    </table>
{% endblock %}", "product_lists.html.twig", "C:\\xampp\\htdocs\\webapps\\hw2eshop\\templates\\product_lists.html.twig");
    }
}
