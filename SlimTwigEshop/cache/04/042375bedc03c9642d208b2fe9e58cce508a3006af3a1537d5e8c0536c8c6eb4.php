<?php

/* product_add_edit_success.html.twig */
class __TwigTemplate_432e83a2473b468d356ebb7029880b3b0b5268619c9efa405a30fc295943d442 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "product_add_edit_success.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Product Submitted";
    }

    // line 3
    public function block_mainContent($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        if (((isset($context["addedit"]) ? $context["addedit"] : null) == 0)) {
            // line 5
            echo "        <p>Product added.</p>
    ";
        } else {
            // line 7
            echo "        <p>Product edited.</p>
    ";
        }
    }

    public function getTemplateName()
    {
        return "product_add_edit_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 7,  41 => 5,  38 => 4,  35 => 3,  29 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}
{% block title %}Product Submitted{% endblock %}
{% block mainContent %}
    {% if addedit == 0 %}
        <p>Product added.</p>
    {% else %}
        <p>Product edited.</p>
    {% endif %}
{% endblock %}
", "product_add_edit_success.html.twig", "C:\\xampp\\htdocs\\webapps\\hw2eshop\\templates\\product_add_edit_success.html.twig");
    }
}
