-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 06, 2017 at 12:20 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eshopdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart_item`
--

CREATE TABLE `cart_item` (
  `id` int(11) NOT NULL,
  `sessionID` varchar(150) NOT NULL,
  `productID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `whenCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cart_item`
--

INSERT INTO `cart_item` (`id`, `sessionID`, `productID`, `quantity`, `whenCreated`) VALUES
(1, 'rtp05letq15sfs7qb0a0m5nvv4', 2, 1, '2017-03-05 17:47:09'),
(8, 'rtp05letq15sfs7qb0a0m5nvv4', 4, 1, '2017-03-05 18:31:53'),
(9, 'rtp05letq15sfs7qb0a0m5nvv4', 3, 1, '2017-03-05 20:14:48'),
(10, '8m0kvshn9d36o64uja9h5ett31', 2, 1, '2017-03-05 23:01:12'),
(14, '8m0kvshn9d36o64uja9h5ett31', 3, 1, '2017-03-05 23:02:25'),
(16, '8m0kvshn9d36o64uja9h5ett31', 4, 1, '2017-03-05 23:19:41'),
(19, 'lh5o43lpiq9i2bbkfdebqh4866', 2, 1, '2017-03-05 23:57:25'),
(20, 'lh5o43lpiq9i2bbkfdebqh4866', 4, 1, '2017-03-06 00:00:42'),
(21, 'lh5o43lpiq9i2bbkfdebqh4866', 3, 1, '2017-03-06 00:01:26'),
(23, 'afo58gfofa620sna40st6b9o83', 2, 1, '2017-03-06 00:02:49'),
(24, 'afo58gfofa620sna40st6b9o83', 3, 1, '2017-03-06 00:06:05'),
(25, 'afo58gfofa620sna40st6b9o83', 4, 1, '2017-03-06 00:06:55'),
(27, '7h4v47asu47fqolfdjp8639io1', 2, 1, '2017-03-06 00:08:17'),
(28, '7h4v47asu47fqolfdjp8639io1', 3, 1, '2017-03-06 00:08:32'),
(29, '7h4v47asu47fqolfdjp8639io1', 4, 1, '2017-03-06 00:20:13'),
(31, 'd1ldlptfbbt0b827rptl59f5g5', 2, 1, '2017-03-06 01:08:53'),
(32, 'd1ldlptfbbt0b827rptl59f5g5', 3, 1, '2017-03-06 01:14:58'),
(33, 'd1ldlptfbbt0b827rptl59f5g5', 4, 1, '2017-03-06 01:17:39'),
(34, '22vt6e1f1tbkggacrg9te6r4i5', 2, 1, '2017-03-06 01:27:05'),
(35, '22vt6e1f1tbkggacrg9te6r4i5', 3, 1, '2017-03-06 01:27:10'),
(36, '22vt6e1f1tbkggacrg9te6r4i5', 4, 1, '2017-03-06 01:30:39'),
(38, 'l60itafkr41819pvnb0ntipl46', 2, 1, '2017-03-06 02:19:07'),
(39, 'l60itafkr41819pvnb0ntipl46', 3, 1, '2017-03-06 02:20:59'),
(40, 'l60itafkr41819pvnb0ntipl46', 4, 1, '2017-03-06 02:21:13');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Memory'),
(2, 'Hard Disk Drive'),
(4, 'Solid State Drive');

-- --------------------------------------------------------

--
-- Table structure for table `order_header`
--

CREATE TABLE `order_header` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country` varchar(50) NOT NULL,
  `provinceorstate` varchar(20) NOT NULL,
  `email` varchar(250) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `credit_card_no` varchar(20) NOT NULL,
  `credit_card_expirity` date NOT NULL,
  `credit_card_cvv` varchar(6) NOT NULL,
  `total_before_tax_and_delivery` decimal(10,2) NOT NULL,
  `delivery` decimal(10,2) NOT NULL,
  `taxes` decimal(10,2) NOT NULL,
  `total_final` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_header`
--

INSERT INTO `order_header` (`id`, `first_name`, `last_name`, `address`, `postcode`, `country`, `provinceorstate`, `email`, `phone`, `credit_card_no`, `credit_card_expirity`, `credit_card_cvv`, `total_before_tax_and_delivery`, `delivery`, `taxes`, `total_final`) VALUES
(2, 'Adolfo', 'Pereira', '291 RUE Medard', 'H7L 5K4', 'Canada', 'Quebec', 'adolfo@adolfo.com', '+15145704339', '555555555555555', '0000-00-00', '233', '784.52', '15.00', '94.14', '893.66'),
(3, 'Adolfo', 'Pereira', 'Laval', 'JJJ JJJ', 'CANADA', 'QUEBEC', 'adolfo@adolfo.com', '333333333333', '444444444444444', '0000-00-00', '222', '119.00', '15.00', '14.28', '148.28'),
(4, 'Adolfo', 'Pereira', 'Laval', 'sdsdsds', 'Canada', 'Quebec', 'adolfo@adolfo.com', '3333333333', '3333333333333333333', '0000-00-00', '222', '287.94', '15.00', '34.55', '337.49'),
(5, 'Adolfo', 'pereira', 'laval', 'wewewew', 'canada', 'quebec', 'adolfo@adolfo.com', '33333333333', '33333333333333333333', '0000-00-00', '333', '176.78', '15.00', '21.21', '212.99'),
(6, 'Adolfo', 'Pereira', 'adsadsad', '4444444', 'Canada', 'quebec', 'adolfo@adolfo.com', '33333333333333', '44444444444444444', '0000-00-00', '333', '176.78', '15.00', '21.21', '212.99');

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE `order_item` (
  `id` int(11) NOT NULL,
  `orderHeaderID` int(11) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `image_path` varchar(200) NOT NULL,
  `unit_price` decimal(10,2) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_item`
--

INSERT INTO `order_item` (`id`, `orderHeaderID`, `category_name`, `name`, `description`, `image_path`, `unit_price`, `quantity`) VALUES
(1, 2, 'Memory', 'Kingston HyperX FURY', 'Kingston HyperX Fury 16GB Kit (2x8GB) 1866MHz DDR3 CL10 DIMM - Blue', '/uploads/63d1e949c80e1a5d77badb9485b414a5.jpg', '119.00', 3),
(2, 2, 'Memory', 'Kingston HyperX FURY', 'Kingston HyperX FURY 16GB Kit (2x8GB) 1866MHz DDR3 CL10 DIMM - White', '/uploads/0663280d437b42590ce24f6e1b86dbf8.jpg', '106.97', 2),
(3, 2, 'Memory', 'Kingston HyperX FURY', 'Kingston HyperX FURY 16GB Kit (2x8GB) 1866MHz DDR3 CL10 DIMM - Red', '/uploads/56279e859163ab80152a0ee544fc8c7c.jpg', '106.79', 2),
(4, 3, 'Memory', 'Kingston HyperX FURY', 'Kingston HyperX Fury 16GB Kit (2x8GB) 1866MHz DDR3 CL10 DIMM - Blue', '/uploads/63d1e949c80e1a5d77badb9485b414a5.jpg', '119.00', 1),
(5, 4, 'Hard Disk Drive', 'WD 2TB Elements Portable External Hard Drive', ' 2TB Storage Capacity\r\nUSB 3.0 and USB 2.0 compatibility\r\nUp to 5 Gb/s Data Transfer Speed\r\nFormatted NTFS for Windows 10, Windows 8.1, Window 7.\r\nEasily Reformat Drive for Mac\r\nUltra-fast USB 3.0 data transfers. Massive capacity ', '/uploads/e2274e642edcc67bfe7a358877663a4b.jpg', '69.99', 1),
(6, 4, 'Hard Disk Drive', 'WD Red 3TB NAS Hard Disk Drive - 5400 RPM Class SATA 6 Gb/s 64MB Cache 3.5 Inch', ' Specifically designed for use in NAS systems with up to 8 bays\r\nTested for 24x7 reliability\r\nNASware firmware for compatibility\r\n3-year limited warranty\r\nSmall and home office NAS systems in a 24x7 enviornment\r\nPackage includes a hard drive only - no screws, cables, manuals included. Please purchase mounting hardware and cables separately if necessary. ', '/uploads/e49723a11f280732bab425fd8be3747d.jpg', '109.00', 1),
(7, 4, 'Solid State Drive', 'Samsung 850 EVO 250GB 2.5-Inch SATA III Internal SSD', ' Powered by Samsung V-NAND Technology; Optimized Performance for Everyday Computing.\r\nIncredible Sequential Read/Write Performance : Up to 540MB/s and 520MB/s Respectively, and Random Read/Write IOPS Performance : Up to 97K and 88K Respectively\r\nEndurance, Reliability, Energy Efficiency, and a 5-Year Limited Warranty\r\nIncluded Contents: 2.5â€ (7mm) SATA III (6GB/s) SSD & User Manual (All Other Cables, Screws, Brackets Not Included).\r\nFree download of Samsung Data Migration and Magician software available for easy installation and SSD management ', '/uploads/15c5c6233e3837b6b8bb806edb68b95e.jpg', '108.95', 1),
(8, 5, 'Memory', 'Kingston HyperX FURY', 'Kingston HyperX FURY 16GB Kit (2x8GB) 1866MHz DDR3 CL10 DIMM - Red', '/uploads/56279e859163ab80152a0ee544fc8c7c.jpg', '106.79', 1),
(9, 5, 'Hard Disk Drive', 'WD 2TB Elements Portable External Hard Drive', ' 2TB Storage Capacity\r\nUSB 3.0 and USB 2.0 compatibility\r\nUp to 5 Gb/s Data Transfer Speed\r\nFormatted NTFS for Windows 10, Windows 8.1, Window 7.\r\nEasily Reformat Drive for Mac\r\nUltra-fast USB 3.0 data transfers. Massive capacity ', '/uploads/e2274e642edcc67bfe7a358877663a4b.jpg', '69.99', 1),
(10, 6, 'Memory', 'Kingston HyperX FURY', 'Kingston HyperX FURY 16GB Kit (2x8GB) 1866MHz DDR3 CL10 DIMM - Red', '/uploads/56279e859163ab80152a0ee544fc8c7c.jpg', '106.79', 1),
(11, 6, 'Hard Disk Drive', 'WD 2TB Elements Portable External Hard Drive', ' 2TB Storage Capacity\r\nUSB 3.0 and USB 2.0 compatibility\r\nUp to 5 Gb/s Data Transfer Speed\r\nFormatted NTFS for Windows 10, Windows 8.1, Window 7.\r\nEasily Reformat Drive for Mac\r\nUltra-fast USB 3.0 data transfers. Massive capacity ', '/uploads/e2274e642edcc67bfe7a358877663a4b.jpg', '69.99', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `categoryID` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `image_path` varchar(200) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `isFrontPage` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `categoryID`, `name`, `description`, `image_path`, `price`, `isFrontPage`) VALUES
(2, 1, 'Kingston HyperX FURY', 'Kingston HyperX Fury 16GB Kit (2x8GB) 1866MHz DDR3 CL10 DIMM - Blue', '/uploads/63d1e949c80e1a5d77badb9485b414a5.jpg', '119.00', 1),
(3, 1, 'Kingston HyperX FURY', 'Kingston HyperX FURY 16GB Kit (2x8GB) 1866MHz DDR3 CL10 DIMM - White', '/uploads/0663280d437b42590ce24f6e1b86dbf8.jpg', '106.97', 1),
(4, 1, 'Kingston HyperX FURY', 'Kingston HyperX FURY 16GB Kit (2x8GB) 1866MHz DDR3 CL10 DIMM - Red', '/uploads/56279e859163ab80152a0ee544fc8c7c.jpg', '106.79', 0),
(5, 1, 'Kingston HyperX FURY', 'Kingston HyperX FURY 16GB Kit (2x8GB) 1866MHz DDR3 CL10 DIMM - Black', '/uploads/bd3fe78ab131988cdfce69a45f4b84d3.jpg', '99.89', 0),
(6, 2, 'WD 2TB Elements Portable External Hard Drive', '2TB Storage Capacity\r\nUSB 3.0 and USB 2.0 compatibility\r\nUp to 5 Gb/s Data Transfer Speed\r\nFormatted NTFS for Windows 10, Windows 8.1, Window 7.\r\nEasily Reformat Drive for Mac\r\nUltra-fast USB 3.0 data transfers. Massive capacity ', '/uploads/e2274e642edcc67bfe7a358877663a4b.jpg', '69.99', 1),
(7, 2, 'WD Red 3TB NAS Hard Disk Drive - 5400 RPM Class SATA 6 Gb/s 64MB Cache 3.5 Inch', ' Specifically designed for use in NAS systems with up to 8 bays\r\nTested for 24x7 reliability\r\nNASware firmware for compatibility\r\n3-year limited warranty\r\nSmall and home office NAS systems in a 24x7 enviornment\r\nPackage includes a hard drive only - no screws, cables, manuals included. Please purchase mounting hardware and cables separately if necessary. ', '/uploads/e49723a11f280732bab425fd8be3747d.jpg', '109.00', 1),
(8, 4, 'Samsung 850 EVO 250GB 2.5-Inch SATA III Internal SSD', ' Powered by Samsung V-NAND Technology; Optimized Performance for Everyday Computing.\r\nIncredible Sequential Read/Write Performance : Up to 540MB/s and 520MB/s Respectively, and Random Read/Write IOPS Performance : Up to 97K and 88K Respectively\r\nEndurance, Reliability, Energy Efficiency, and a 5-Year Limited Warranty\r\nIncluded Contents: 2.5â€ (7mm) SATA III (6GB/s) SSD & User Manual (All Other Cables, Screws, Brackets Not Included).\r\nFree download of Samsung Data Migration and Magician software available for easy installation and SSD management ', '/uploads/15c5c6233e3837b6b8bb806edb68b95e.jpg', '108.95', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart_item`
--
ALTER TABLE `cart_item`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_index` (`sessionID`,`productID`),
  ADD KEY `sessionID` (`sessionID`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_header`
--
ALTER TABLE `order_header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orderHeaderID` (`orderHeaderID`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoryID` (`categoryID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart_item`
--
ALTER TABLE `cart_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `order_header`
--
ALTER TABLE `order_header`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `order_item`
--
ALTER TABLE `order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
